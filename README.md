# mGBA Script: Pokemon Species Base Stat Reader

Read Pokemon species data from a currently running ROM and write the data into a file or mGBA's stdout (Standard Output) stream.

Note: Currently, only Emerald (USA), FireRed (USA) and LeafGreen (USA) should work, Contributions are welcome though!

## Output formats

1: Some human readable format:

```
#X: species name

hp    = X
atk   = X
def   = X
spatk = X
spdef = X
speed = X

total = X

type1 = X
type2 = X

#X: species name

hp    = X
atk   = X
def   = X
spatk = X
spdef = X
speed = X

total = X

type1 = X
type2 = X
```

2: Almost valid JSON

```
{ "total": "X","spatk": "X","atk": "X","type2": "X","speed": "X","hp": "X","spdef": "X","index": "X","type1": "X","def": "X" },
{ "total": "X","spatk": "X","atk": "X","type2": "X","speed": "X","hp": "X","spdef": "X","index": "X","type1": "X","def": "X" },
```

Note: To make this valid JSON one should add a `[` before and after it and from the last line's `},` remove the last trailing `,` so that the last line ends in `}`

```json
[{ "total": "X", "name": "X", "spatk": "X", "atk": "X", "type2": "X", "speed": "X", "spdef": "X", "hp": "X", "index": "X", "type1": "X", "def": "X" },
{ "total": "X", "name": "X", "spatk": "X", "atk": "X", "type2": "X", "speed": "X", "spdef": "X", "hp": "X", "index": "X", "type1": "X", "def": "X" }]
```

This is easy to do manually or automate externally as it only requires adding a leading `[` and removing the last `,` and adding a trailing `]` (replacing the last `,` character with a `]`)

Fixed JSON prettified without sorting:

```json
[
  {
    "total": "X",
    "name": "X",
    "spatk": "X",
    "atk": "X",
    "type2": "X",
    "speed": "X",
    "spdef": "X",
    "hp": "X",
    "index": "X",
    "type1": "X",
    "def": "X"
  },
  {
    "total": "X",
    "name": "X",
    "spatk": "X",
    "atk": "X",
    "type2": "X",
    "speed": "X",
    "spdef": "X",
    "hp": "X",
    "index": "X",
    "type1": "X",
    "def": "X"
  }
]
```

New output formats are fairly easy to write in `output(index, hp, atk, def, spatk, spdef, speed, total, type1, type2)`

## How to run

Check the editable variables at the start of the script file.

Editable variables:

```lua
-- write to a file instead of mGBA stdout?
WRITETOAFILE = false -- `true` or `false`
-- filename if we do write to a file
FILENAME = "mgba-exported-base-stats.txt"
-- "a" Append, doesn't rewrite the file but keeps the old contents
-- "w" (re)Write, doesn't append to the file nor keep the old contents
FILEOPENTYPE = "a"



OUTPUTFORMAT = 1



-- Which Pokemon should have their data printed?
-- if digits match then only that mon is read,
-- otherwise it'll be a range from first to last
-- 1 and 9 read Pokemons 1, 2, 3, 4, 5, 6, 7, 8 and 9
-- 123 and 123 only read the Pokemon #123
FIRSTPOKEMONTOREAD = 123
LASTPOKEMONTOREAD  = 123
```

Warning: there are little to no safeguards for validating what Pokemon you will read. If you attempt to read Pokemon #1337 you will be reading random garbage!

After configuring the script you can run it.

Open mGBA and in the toolbar select Tools > Scripting... and in the new window select in its toolbar File > load script... and find this project's files with your file browser.

If you don't know how then follow the "How to run" from this github repository: https://github.com/nikouu/mGBA-lua-HelloWorld

## Pokemon data structure references used

https://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_species_data_structure_(Generation_III)

## mGBA provided example script used as a base

https://github.com/mgba-emu/mgba/blob/master/res/scripts/pokemon.lua

## Function flow explained in a nutshell:

1. `readStats()` root of the functionality, call this to read stats

2. `openfile()` if WRITETOAFILE is true then a FILENAME file is opened for writing

3. readStats gathers the necessary data and then calls the next function

4. `output(index, hp, atk, def, spatk, spdef, speed, total, type1, type2)`

  This function handles formatting the data to into an output format

5. `write(content)` writes content to a file or to the stdout stream

`closefile()` if WRITETOAFILE is true then FILENAME file is closed
