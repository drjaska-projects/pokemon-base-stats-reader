
-- -------------------------------
-- - START OF EDITABLE VARIABLES -
-- -------------------------------

-- write to a file instead of mGBA stdout?
WRITETOAFILE = false -- `true` or `false`
-- filename if we do write to a file
FILENAME = "mgba-exported-base-stats.txt"
-- "a" Append, doesn't rewrite the file but keeps the old contents
-- "w" (re)Write, doesn't append to the file nor keep the old contents
FILEOPENTYPE = "a"

--[[ Formats:
  1:

#X: species name

hp    = X
atk   = X
def   = X
spatk = X
spdef = X
speed = X

total = X

type1 = X
type2 = X

#X: species name

hp    = X
atk   = X
def   = X
spatk = X
spdef = X
speed = X

total = X

type1 = X
type2 = X

  2:
{ "total": "X", "name": "X", "spatk": "X", "atk": "X", "type2": "X", "speed": "X", "spdef": "X", "hp": "X", "index": "X", "type1": "X", "def": "X" },
{ "total": "X", "name": "X", "spatk": "X", "atk": "X", "type2": "X", "speed": "X", "spdef": "X", "hp": "X", "index": "X", "type1": "X", "def": "X" },

Note: To make this valid JSON one should add a [ before and after it and from the last line's `},` remove the last trailing , so that the last line ends in }

[{ "total": "X","spatk": "X","atk": "X","type2": "X","speed": "X","hp": "X","spdef": "X","index": "X","type1": "X","def": "X" },
{ "total": "X","spatk": "X","atk": "X","type2": "X","speed": "X","hp": "X","spdef": "X","index": "X","type1": "X","def": "X" }]

This is easy to do manually or automate externally as it only requires adding a leading [ and removing the last , and adding a trailing ] (replacing the last character with a ])

--]]
OUTPUTFORMAT = 1

-- Which Pokemon should have their data printed?
-- if digits match then only that mon is read,
-- otherwise it'll be a range from first to last
-- 1 and 9 read Pokemons 1, 2, 3, 4, 5, 6, 7, 8 and 9
-- 123 and 123 only read the Pokemon #123
FIRSTPOKEMONTOREAD = 123
LASTPOKEMONTOREAD  = 123

-- -----------------------------
-- - END OF EDITABLE VARIABLES -
-- -----------------------------

-- ----------------------------------------------------------------------------------------
-- Base structure from https://github.com/mgba-emu/mgba/blob/master/res/scripts/pokemon.lua
-- ----------------------------------------------------------------------------------------

-- TODO: find unnecessary data from the base structure from the repurposing and rip it out

local Game = {
	new = function (self, game)
		self.__index = self
		setmetatable(game, self)
		return game
	end
}

function Game.toString(game, rawstring)
	local string = ""
	for _, char in ipairs({rawstring:byte(1, #rawstring)}) do
		if char == game._terminator then
			break
		end
		string = string..game._charmap[char]
	end
	return string
end

function Game.getSpeciesName(game, id)
	if game._speciesIndex then
		local index = game._index
		if not index then
			index = {}
			for i = 0, 255 do
				index[emu.memory.cart0:read8(game._speciesIndex + i)] = i
			end
			game._index = index
		end
		id = index[id]
	end
	local pointer = game._speciesNameTable + (game._speciesNameLength) * id
	return game:toString(emu.memory.cart0:readRange(pointer, game._monNameLength))
end

local GBGameEn = Game:new{
	_terminator=0x50,
	_monNameLength=10,
	_speciesNameLength=10,
	_playerNameLength=10,
}

local GBAGameEn = Game:new{
	_terminator=0xFF,
	_monNameLength=10,
	_speciesNameLength=11,
	_playerNameLength=10,
}

local Generation1En = GBGameEn:new{
	_boxMonSize=33,
	_partyMonSize=44,
}

local Generation2En = GBGameEn:new{
	_boxMonSize=32,
	_partyMonSize=48,
}

local Generation3En = GBAGameEn:new{
	_boxMonSize=80,
	_partyMonSize=100,
}

GBGameEn._charmap = { [0]=
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", " ",
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
	"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "(", ")", ":", ";", "[", "]",
	"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
	"q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "é", "ʼd", "ʼl", "ʼs", "ʼt", "ʼv",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
	"'", "P\u{200d}k", "M\u{200d}n", "-", "ʼr", "ʼm", "?", "!", ".", "ァ", "ゥ", "ェ", "▹", "▸", "▾", "♂",
	"$", "×", ".", "/", ",", "♀", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
}

GBAGameEn._charmap = { [0]=
	" ", "À", "Á", "Â", "Ç", "È", "É", "Ê", "Ë", "Ì", "こ", "Î", "Ï", "Ò", "Ó", "Ô",
	"Œ", "Ù", "Ú", "Û", "Ñ", "ß", "à", "á", "ね", "ç", "è", "é", "ê", "ë", "ì", "ま",
	"î", "ï", "ò", "ó", "ô", "œ", "ù", "ú", "û", "ñ", "º", "ª", "�", "&", "+", "あ",
	"ぃ", "ぅ", "ぇ", "ぉ", "v", "=", "ょ", "が", "ぎ", "ぐ", "げ", "ご", "ざ", "じ", "ず", "ぜ",
	"ぞ", "だ", "ぢ", "づ", "で", "ど", "ば", "び", "ぶ", "べ", "ぼ", "ぱ", "ぴ", "ぷ", "ぺ", "ぽ",
	"っ", "¿", "¡", "P\u{200d}k", "M\u{200d}n", "P\u{200d}o", "K\u{200d}é", "�", "�", "�", "Í", "%", "(", ")", "セ", "ソ",
	"タ", "チ", "ツ", "テ", "ト", "ナ", "ニ", "ヌ", "â", "ノ", "ハ", "ヒ", "フ", "ヘ", "ホ", "í",
	"ミ", "ム", "メ", "モ", "ヤ", "ユ", "ヨ", "ラ", "リ", "⬆", "⬇", "⬅", "➡", "ヲ", "ン", "ァ",
	"ィ", "ゥ", "ェ", "ォ", "ャ", "ュ", "ョ", "ガ", "ギ", "グ", "ゲ", "ゴ", "ザ", "ジ", "ズ", "ゼ",
	"ゾ", "ダ", "ヂ", "ヅ", "デ", "ド", "バ", "ビ", "ブ", "ベ", "ボ", "パ", "ピ", "プ", "ペ", "ポ",
	"ッ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "!", "?", ".", "-", "・",
	"…", "“", "”", "‘", "’", "♂", "♀", "$", ",", "×", "/", "A", "B", "C", "D", "E",
	"F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
	"V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
	"l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "▶",
	":", "Ä", "Ö", "Ü", "ä", "ö", "ü", "⬆", "⬇", "⬅", "�", "�", "�", "�", "�", ""
}

--[[
https://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_species_data_structure_(Generation_III)
Ruby            0x081FEC34 -- Japanese or US??
Sapphire        0x081FEBC4 -- Japanese or US??
Emerald         0x082F0D70
EmeraldEN-US    0x083203E8
FireRed         0x082111A8
FireRedEN-US    0x082547A0
LeafGreen       0x08211184
LeafGreenEN-US  0x0825477C
--]]

--[[ Untested Games
local gameRBEn = Generation1En:new{
	name="Red/Blue (USA)",
	_party=0xd16b,
	_partyCount=0xd163,
	_partyNames=0xd2b5,
	_partyOt=0xd273,
	_speciesNameTable=0x1c21e,
	_speciesIndex=0x41024,
}

local gameYellowEn = Generation1En:new{
	name="Yellow (USA)",
	_party=0xd16a,
	_partyCount=0xd162,
	_partyNames=0xd2b4,
	_partyOt=0xd272,
	_speciesNameTable=0xe8000,
	_speciesIndex=0x410b1,
}

local gameGSEn = Generation2En:new{
	name="Gold/Silver (USA)",
	_party=0xda2a,
	_partyCount=0xda22,
	_partyNames=0xdb8c,
	_partyOt=0xdb4a,
	_speciesNameTable=0x1b0b6a,
}

local gameCrystalEn = Generation2En:new{
	name="Crystal (USA)",
	_party=0xdcdf,
	_partyCount=0xdcd7,
	_partyNames=0xde41,
	_partyOt=0xddff,
	_speciesNameTable=0x5337a,
}
--]]
--[[ No idea if the addresses on Bulbapedia are for jp or en or enr1 releases
local gameRubyEn = Generation3En:new{
	name="Ruby (USA)",
	_party=0x3004360,
	_partyCount=0x3004350,
	_speciesNameTable=0x1f716c,
}

local gameRubyEnR1 = Generation3En:new{
	name="Ruby (USA)",
	_party=0x3004360,
	_partyCount=0x3004350,
	_speciesNameTable=0x1f7184,
}

local gameSapphireEn = Generation3En:new{
	name="Sapphire (USA)",
	_party=0x3004360,
	_partyCount=0x3004350,
	_speciesNameTable=0x1f70fc,
}

local gameSapphireEnR1 = Generation3En:new{
	name="Sapphire (USA)",
	_party=0x3004360,
	_partyCount=0x3004350,
	_speciesNameTable=0x1f7114,
}
--]]
local gameEmeraldEn = Generation3En:new{
	name="Emerald (USA)",
	_speciesDataTable=0x083203E8,
	_speciesNameTable=0x3185c8,
}

local gameFireRedEn = Generation3En:new{
	name="FireRed (USA)",
	_speciesDataTable=0x082547A0,
	_speciesNameTable=0x245ee0,
}
--[[ Untested Games
local gameFireRedEnR1 = gameFireRedEn:new{
	name="FireRed (USA) (Rev 1)",
	_speciesNameTable=0x245f50,
}
--]]
-- Might work, Bulbapedia had the data table address
local gameLeafGreenEn = Generation3En:new{
	name="LeafGreen (USA)",
	_speciesDataTable=0x0825477C,
	_speciesNameTable=0x245ebc,
}
--[[ lacks data table address
local gameLeafGreenEnR1 = gameLeafGreenEn:new{
	name="LeafGreen (USA)",
	_party=0x2024284,
	_partyCount=0x2024029,
	_speciesNameTable=0x245f2c,
}
--]]
gameCodes = {
--	["DMG-AAUE"]=gameGSEn, -- Gold
--	["DMG-AAXE"]=gameGSEn, -- Silver
--	["CGB-BYTE"]=gameCrystalEn,
--	["AGB-AXVE"]=gameRubyEn,
--	["AGB-AXPE"]=gameSapphireEn,
	["AGB-BPEE"]=gameEmeraldEn,
	["AGB-BPRE"]=gameFireRedEn,
	["AGB-BPGE"]=gameLeafGreenEn,
}

-- These versions have slight differences and/or cannot be uniquely
-- identified by their in-header game codes, so fall back on a CRC32
gameCrc32 = {
--	[0x9f7fdd53] = gameRBEn, -- Red
--	[0xd6da8a1a] = gameRBEn, -- Blue
--	[0x7d527d62] = gameYellowEn,
--	[0x84ee4776] = gameFireRedEnR1,
--	[0xdaffecec] = gameLeafGreenEnR1,
--	[0x61641576] = gameRubyEnR1, -- Rev 1
--	[0xaeac73e6] = gameRubyEnR1, -- Rev 2
--	[0xbafedae5] = gameSapphireEnR1, -- Rev 1
--	[0x9cc4410e] = gameSapphireEnR1, -- Rev 2
}

function detectGame()
	local checksum = 0
	for i, v in ipairs({emu:checksum(C.CHECKSUM.CRC32):byte(1, 4)}) do
		checksum = checksum * 256 + v
	end
	game = gameCrc32[checksum]
	if not game then
		game = gameCodes[emu:getGameCode()]
	end

	if not game then
		console:error("Unknown game!")
	else
		console:log("Found game: " .. game.name)
		-- if not partyBuffer then
		--	partyBuffer = console:createBuffer("Party")
		-- end
	end
end

callbacks:add("start", detectGame)
if emu then
	detectGame()
end



-- -------------------------
-- - End of base structure -
-- -------------------------



Generation3TypeChart = {
	[00]="normal",
	[01]="fighting",
	[02]="flying",
	[03]="poison",
	[04]="ground",
	[05]="rock",
	[06]="bug",
	[07]="ghost",
	[08]="steel",
	[09]="???",
	[10]="fire",
	[11]="water",
	[12]="grass",
	[13]="electric",
	[14]="psychic",
	[15]="ice",
	[16]="dragon",
	[17]="dark",
}

-- "unknown custom type X" for unindexed types,
-- covers all custom types without this table being 255 entries long

local metatable = {}

function metatable.__index(table, key)
   return "unknown custom type " .. key
end

setmetatable(Generation3TypeChart, metatable)

-- initialise file operations
function openfile()
	if WRITETOAFILE and not file then
		file = io.open(FILENAME, FILEOPENTYPE)
	end
end

-- end file operations
function closefile()
	if WRITETOAFILE and file then
		io.close(file)
		file = nil
	end
end

-- decide between file writing and stdout printing
function write(content)
	if WRITETOAFILE and file then
		file:write(content .. "\n")
	elseif not WRITETOAFILE then
		print(content)
	end
end

-- https://stackoverflow.com/a/27028488
-- almost valid JSON, JSON-ish
function producejsonish(o)
	if type(o) == 'table' then
		local s = '{ '
		for k,v in pairs(o) do
			--if type(k) ~= 'number' then k = '"'..k..'"' end
			s = s .. '"'..k..'": "' .. producejsonish(v) .. '", '
		end
		s = s:sub(1, -3) -- remove the last ,
		return s .. ' },'
	else
		return tostring(o)
	end
end

-- Information exporter, mGBA stdout printer or file writer
function output(index, hp, atk, def, spatk, spdef, speed, total, type1, type2)
	if OUTPUTFORMAT == 1 then
		write("")
		write("#" .. index .. ": " .. game:getSpeciesName(index))
		write("")
		write("hp    = " .. tostring(hp))
		write("atk   = " .. tostring(atk))
		write("def   = " .. tostring(def))
		write("spatk = " .. tostring(spatk))
		write("spdef = " .. tostring(spdef))
		write("speed = " .. tostring(speed))
		write("")
		write("total = " .. tostring(total))
		write("")
		write("type1 = " .. type1)
		write("type2 = " .. type2)

	elseif OUTPUTFORMAT == 2 then
		local mon = {}
		mon.index = index
		mon.name = game:getSpeciesName(index)
		mon.hp = hp
		mon.atk = atk
		mon.def = def
		mon.spatk = spatk
		mon.spdef = spdef
		mon.speed = speed
		mon.total = total
		mon.type1 = type1
		mon.type2 = type2

		write(producejsonish(mon))
	end
end

-- "main" function
function readStats()
	openfile()

	-- check that we have a valid range
	if LASTPOKEMONTOREAD < FIRSTPOKEMONTOREAD then
		write("ERROR: LASTPOKEMONTOREAD can NOT be smaller than FIRSTPOKEMONTOREAD")
	else
		-- read all the mons from the range
		for i = FIRSTPOKEMONTOREAD, LASTPOKEMONTOREAD do

			local table = game._speciesDataTable + (28 * (i-1))

			local hp    = emu.memory.cart0:read8(table+0)
			local atk   = emu.memory.cart0:read8(table+1)
			local def   = emu.memory.cart0:read8(table+2)
			local speed = emu.memory.cart0:read8(table+3)
			local spatk = emu.memory.cart0:read8(table+4)
			local spdef = emu.memory.cart0:read8(table+5)

			local total = hp + atk + def + spatk + spdef + speed

			local type1 = Generation3TypeChart[emu.memory.cart0:read8(table+6)]
			local type2 = Generation3TypeChart[emu.memory.cart0:read8(table+7)]

			output(i, hp, atk, def, spatk, spdef, speed, total, type1, type2)
		end
	end

	closefile()
end

-- if a known game is open when this script is loaded, run readStats()
if game then
	readStats()
end
